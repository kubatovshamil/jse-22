package ru.t1.kubatov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractModel {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String email;

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    public boolean isLocked() {
        return this.locked == true;
    }

}
