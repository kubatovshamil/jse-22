package ru.t1.kubatov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.api.model.ICommand;
import ru.t1.kubatov.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public final static String DESCRIPTION = "Display list of terminal commands.";

    @NotNull
    public final static String NAME = "help";

    @NotNull
    public final static String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            System.out.println(command.toString());
        }
    }
}
