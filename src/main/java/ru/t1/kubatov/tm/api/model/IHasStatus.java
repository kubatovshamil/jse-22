package ru.t1.kubatov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
