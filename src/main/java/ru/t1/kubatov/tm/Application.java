package ru.t1.kubatov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kubatov.tm.component.Bootstrap;

public class Application {

    public static void main(@Nullable String... args) {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}